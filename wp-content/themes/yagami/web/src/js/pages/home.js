import { TimelineMax, Power3 } from "gsap";

export default {
	init: (app, Menu, Kira, Swiper, SplitText) => {
        app.dump('home.js');

        const
        $body               = $('body'),
        $siteContainer      = document.getElementById('site-container'),
        ajaxLoaded          = $body.hasClass('ajax-loaded');


        var mySwiper = new Swiper('.swiper-home', {
            direction: 'vertical',
            height: 400,
            simulateTouch: false,
        });


        function debounce(func, wait, immediate) {
            var timeout;
            return function() {
                var context = this, args = arguments;
                var later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        };

        var up = false;

        var myEfficientFn = debounce(function(e) {
            if(e.originalEvent.wheelDelta < 0){
                up = false;
                mySwiper.slideNext();
            } else {
                up = true;
                mySwiper.slidePrev()
            }
        },250 ,true);
        
        $(window).on('mousewheel', myEfficientFn);

        var arrayTl = [];
    
        $('.swiper-slide').each(function(index){
            var images = $(this).find('.home-images');
            var title = $(this).find('.home-title');
            var number = $("[data-loop='" + index + "']");

            var splitTextTitle = new SplitText(title, {type:"words, chars", charsClass:"char"}); 
            var titleChars = splitTextTitle.chars;

            var splitTextNumber = new SplitText(number, {type:"words, chars", charsClass:"char"}); 
            var numberChars = splitTextNumber.chars;

            var tlHome = new TimelineMax({paused: true});

            tlHome.staggerFromTo(images, .2, {autoAlpha: 0, y: 300}, {autoAlpha: 1, y: 0, ease: Power4.easeout}, 0.15, 'start');
            tlHome.staggerFromTo(numberChars, .5, {opacity: 0, y: -40}, {opacity: 1, y: 0, ease: Power4.easeout}, 0.3, 'start');
            tlHome.staggerFromTo(titleChars, .4, {opacity: 0, y: -50}, {opacity: 1, y: 0, ease: Power4.easeout}, 0.045);

            arrayTl.push(tlHome);
        }); 

        arrayTl[0].play();

        mySwiper.on('slideChange', function(){
            
            var activeSlide = mySwiper.activeIndex;
            var prevSlide = mySwiper.previousIndex;

            if(up){
                arrayTl[prevSlide].staggerFromTo($('.swiper-slide-active').find('.home-images'), .2, {autoAlpha: 0, y: 300}, {autoAlpha: 1, y: 0, ease: Power4.easeout}, 0.15, 'start');
            } else {
                arrayTl[prevSlide].staggerFromTo($('.swiper-slide-active').find('.home-images'), .2, {autoAlpha: 0, y: -300}, {autoAlpha: 1, y: 0, ease: Power4.easeout}, 0.15, 'start');
            }

            arrayTl[prevSlide].reverse();

            arrayTl[prevSlide].eventCallback('onReverseComplete', function () { 
                arrayTl[activeSlide].play();
            })

        })

	}
}

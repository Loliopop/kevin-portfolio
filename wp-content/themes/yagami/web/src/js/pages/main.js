export default {
	init: (app, CookieManager, Cursor, MobileDetect, TweenMax, BarbaManager) => {
		/*
		|
		| Constants
		|-----------
		*/
        const 
            $body         = $('body'),
            $affectedByMobile   = $('body, .site-container')
		;



        /*
        |
        | Mobile Detect
        |----------------
        */
        let isMobile   = null;

        let mobileDetector = new MobileDetect();
        isMobile = mobileDetector.isMobile();
        if (isMobile == null) {
            $affectedByMobile.addClass('isDesktop');
        } else {
            $affectedByMobile.addClass('isMobile');
        }



        /*
        |
        | Cursor
        |----------
        */
        if (!isMobile) { 
            new Cursor($('.cursor'), {
                'mouseFollow': {
                    'tween': (cursor, x, y) => {
                        TweenMax.to(cursor, .5, {
                            x: x,
                            y: y,
                            ease: Power3.easeOut
                        })
                    }
                },
                'mouseOver': {
                    'scale': 8,
                    'duration': .5
                },
                'targets': [
                    {
                        'selector': $('a, .gform_footer'),
                        'timeline': (timeline, $cursorInner) => {
                            timeline.to($cursorInner, .5, {
                                scale: 2,
                                ease: Power3.easeOut
                            })
                        }
                    }
                ]
            });
        } else {
            $('.cursor').hide();
        }



        /*
		|
		| BarbaManager
		|--------------
        */
       let barbaManager = new BarbaManager({
            prefetch: false,
            xhrTimeout: 15000,
            onReady: (currentStatus, prevStatus, $newContainer, newPageRawHTML) => {

                const $fullSiteWrapper = $newContainer.find('.full-site-wrapper');
                const $fullSiteLayer = $newContainer.find('.full-site-layer');

                $fullSiteLayer.addClass('positionned');

            },
            onLeave: ($oldContainer, timeline, clickedLink) => {
                let type = 'normal';
                const
                    $fullSiteWrapper = $oldContainer.find('.full-site-wrapper')
                ;
            }
        });


        $body.on('loaderEnd', () => console.log('ended'))
	}
}
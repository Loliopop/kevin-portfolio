class Cursor{


    constructor($cursor, params = {}) {
        this.cursor = $cursor;
        this.cursorShape = $('.cursor-shape');
        this.cursorInner = $('.cursor-inner');
        this.cursorText = $('.cursor-text');
        this.lien = $('a, input');

        this.data;

        this.scaleCursor;

        this.params = this.initParams(params);
        this.createTimeline();
        this.mouseFollow();
    }




	initParams(params){
        const { mouseFollow, mouseOver, targets } = params;
        const { duration, tween, easing } = mouseFollow;
        const { scale, overDuration } = mouseOver;


		return {
			'tween': this.isDefined(tween) ? tween : null,
            'duration': this.isDefined(duration) ? duration : 2,
            'easing': this.isDefined(easing) ? easing : Power3.easeOut,

            'scale': this.isDefined(scale) ? scale : 3,
            'overDuration': this.isDefined(overDuration) ? overDuration : 1,

            'targets' : targets,
		}
	}

    mouseFollow() {
        const { tween, duration, easing } = this.params;

            $(window).on('mousemove', (e) => {
                let x = e.originalEvent.clientX;
                let y = e.originalEvent.clientY;

                if(tween === null){
                    TweenLite.to(this.cursor, duration, { left: x, top: y, ease: easing })
                } else {
                    tween(this.cursor, x, y)
                }
            })

        $(window).on('mousemove', e => {
            let x = e.originalEvent.clientX;
            let y = e.originalEvent.clientY;

            if(tween === null){
                TweenLite.to(this.cursor, duration, { left: x, top: y, ease: easing })
            } else {
                tween(this.cursor, x, y)
            }
            
        })
    }

    dispachEvent($element, eventName, datas = null) {
        var event = $.Event(eventName);

        if (datas !== null) {
            $.each(datas, function (key, value) {
                event[key] = value
            });
        }

        $element.trigger(event);
    }

    createTimeline(){
        const { targets } = this.params;
        const _this = this;
        var timelines = [];
        targets.map( (element, i) => {
            var selector = element.selector;

            var timelineOver = new TimelineMax({ paused: true });
            element.timeline(timelineOver, this.cursorInner, this.cursorText, selector);
            timelines.push(timelineOver);

            selector.on('mouseover', function(e){
                timelines.map(timeline => {
                    timeline.reverse().timeScale(5);
                })
                timelines[i].play().timeScale(1);

                _this.dispachEvent($(window), 'cursor:current', {
                    hovered: $(this)
                });
            })
            selector.on('mouseleave', function(){
                timelines.map(timeline => {
                    timeline.reverse().timeScale(5);
                })
            })

        })
    }


    /**
	|
	| Helper: isDefined
	|--------------------
	|
	*/
    isDefined(item) {
        return typeof item !== 'undefined';
    }
}

export default Cursor;
